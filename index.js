// soal no 1
let keliling = (p,l) => 2+(p * l);
let luas = (p,l) => p * l;
console.log(keliling (2,3));
console.log(luas (2,3));

//soal no 2
const fullname = 'William Imoh'
const william = {fullname}

//soal no 3
const newObject = {
  firstName: "Muhammad",
  lastName: "Iqbal Mubarok",
  address: "Jalan Ranamanyar",
  hobby: "playing football",
}
const {firstName, lastName,address,hobby} = newObject
console.log(firstName, lastName, address, hobby)

//soal no 4
const west = ["Will", "Chris", "Sam", "Holly"]
const east = ["Gill", "Brian", "Noel", "Maggie"]
let combinedArray = [...west, ...east]
console.log(combinedArray) // ['one', 'two', 'three', 'four', 'five', 'six']

//soal no 5
const planet = "earth" 
const view = "glass" 
var before = 'Lorem ' + view + 'dolor sit amet, ' + 'consectetur adipiscing elit,' + planet 